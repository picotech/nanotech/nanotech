# From Imports
from nanite import fullpath
from os import path as os_path, stat
from typing import List, Tuple, Union, Any, MutableSequence as MS

# Local Imports
from nanotech.etc import list_to_file, sn_to_float


class Error(Exception):
	pass


class no_check(Error):
	pass


class no_dir(Error):
	pass


class hash_not_installed(Error):
	pass


class no_hash(Error):
	pass


def _compare_and_write_to_file(
	hash_file: str, hash_list: List[str], dry_run: bool
) -> bool:
	file_len: int = 0
	with open(hash_file, "r") as file:
		for line in file:
			file_len = file_len + 1

	if len(hash_list) != file_len:
		if dry_run:
			return True
		else:
			with open(hash_file, "w") as file:
				for _hash in hash_list:
					file.write(_hash + "\n")
			return True
	else:
		with open(hash_file, "r+") as file:
			for hash_line, file_line in zip(hash_list, file):
				file_line = file_line.rstrip()
				if hash_line != file_line:
					if dry_run:
						return True
					else:
						file.seek(0)
						for _hash in hash_list:
							file.write(_hash + "\n")
						return True
		return False


def fd(
	time: Union[int, str],
	source: str,
	tz: str = "Canada/Eastern",
	tmp_dir: str = "/tmp",
	ignore_file: str = "",
	ignore_list: MS[Any] = [],
	follow_symlinks: bool = False,
) -> bool:
	"""
		Returns a False if the "fd" shell command returns an empty list, else a True if it returns a list of changed files

		Variable "fd_st_stdout" uses the output from the shell command, using the "time" and "ignore_file" arguments, as well as the class' "source" argument, to procure a list of changed files within the given time period (in miutes). The unformatted output is "utf-8" decoded, then split into a list.
	"""

	from nanite import peek

	if not ignore_file:
		ltf = list_to_file()
		ignore_file = ltf(ignore_list, tmp_dir=tmp_dir, tz=tz)

	# Don't set the check variable of fullpath here; it checks if the path leads to directory, and else raises an error.
	source = fullpath(source)
	try:
		if os_path.isdir(source):
			from bakery import fd as new_fd

			fd_st_stdout = new_fd(
				".",
				follow=follow_symlinks,
				absolute_path=True,
				hidden=True,
				no_ignore_vcs=True,
				changed_within=f"{time}min",
				ignore_file=fullpath(ignore_file),
				_end_args=[{"value": source, "quotes": True}],
			).stdout
			return peek(
				fd_st_stdout, return_first=2, return_whole=False
			).first
		else:
			raise no_dir(
				f"Sorry! {source} doesn't seem to exist!"
			)
	finally:
		if not ignore_file:
			ltf.clean()


def vc(
	_vc: str, source: str, hash_file: str, dry_run: bool
) -> bool:
	"""
		Temp
	"""

	from functools import partial
	from nanite import sui

	source = fullpath(source)

	vc_exts_names: Dict[str, str] = {
		"hg": (".hg", "mercurial"),
		"git": (".git", "git"),
	}
	repo_ext, vc_name = vc_exts_names[_vc]

	if os_path.isdir(fullpath(source, repo_ext)):
		hash_file = fullpath(hash_file)
		vcf: type = sui("bakery", _vc)
		vc_func_partials = {
			"hg": partial(
				vcf.st,
				repository={"value": source, "quotes": True},
			),
			"git": partial(
				vcf.status,
				# "_before_kwargs" is needed here because "args" and "kwargs" are placed after the
				# subcommand, i.e. "status" in this case
				_before_kwargs={
					"C": {"value": source, "quotes": True}
				},
				porcelain=True,
			),
		}

		vc_st = vc_func_partials[_vc]()
		hash_list = [line for line in vc_st.stdout]
		return _compare_and_write_to_file(
			hash_file, hash_list, dry_run
		)
	else:
		raise no_dir(
			f"Sorry! {source} doesn't seem to be a {vc_name} repository!"
		)


def hash(
	hash_type: str,
	hash_file: str,
	include_list: MS[Any] = [],
	include_file: str = "",
	chunk_size: int = sn_to_float("8.389e+6"),
	dry_run: bool = False,
	x64: bool = False,
) -> bool:
	"""
		Temp
	"""

	from nanite import module_installed

	def module_installed_load(module):
		if hash_type == module:
			_ = module_installed(module)
			if not _:
				raise hash_not_installed(
					f'Sorry; hash type "{hash_type}" not installed! Please install it using pip!'
				)
			return _
		else:
			False

	ihd: Dict[
		str, Tuple[Union[str, Union[Callable[..., Any], bool]]]
	] = {
		"xxhash": module_installed_load("xxhash"),
		"imohash": module_installed_load("imohash"),
	}

	if hash_type not in ihd.keys():
		raise no_hash(
			f'Sorry; hash type {hash_type} does not exist! Please choose from: [{" ".join(ihd.keys())}]'
		)

	if hash_type == "xxhash":
		xxh = getattr(ihd["xxhash"], "xxh64" if x64 else "xxh32")

	hash_list: List[str] = []
	hash_file = fullpath(hash_file)

	def xxhash_file():
		h = xxh()
		with open(line, "rb") as file:
			# chunk_size is in bits
			for chunk in iter(
				lambda: file.read(chunk_size), b""
			):
				h.update(chunk)
		return h.hexdigest()

	def hash_append(file):
		for line in file:
			line = line.rstrip()
			if os_path.isfile(line):
				if hash_type == "xxhash":
					hash_list.append(xxhash_file())
				elif hash_type == "imohash":
					hash_list.append(
						ihd[hash_type].hashfile(
							line, hexdigest=True
						)
					)
			elif os_path.isdir(line):
				hash_list.append(line.rstrip())

	if (
		not include_file
		or stat(inclusion_rules_file).st_size == 0
	):
		hash_append(include_list)
	else:
		with open(fullpath(include_file)) as file:
			hash_append(file)

	return _compare_and_write_to_file(
		hash_file, hash_list, dry_run
	)

