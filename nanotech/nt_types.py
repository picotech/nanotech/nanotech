# Nanotech Imports
from nanite import fullpath, module_installed

# Debug Imports
pout = module_installed("pout")


class ntp:
	"""
		Return fullpath(path)
	"""

	def __init__(self, *args, **kwargs):
		self._ = fullpath(*args, **kwargs)

	def __str__(self):
		return self._


class ntf(ntp):
	"""
		Returns fullpath(filepath)
	"""

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def __str__(self):
		return super.__str__()
