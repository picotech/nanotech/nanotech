#!/usr/bin/env python

# Imports
import click
import os
import schedule
import time

# From Imports
from bakery import tail
from dataclasses import dataclass, field
from loguru import logger
from typing import List, Any, Callable
from nanite import fullpath


# Local Imports
from nanite import Command


@click.group(invoke_without_command=True, no_args_is_help=True)
@logger.catch
def main():
	pass


@click.command(
	name="tail", cls=Command, no_args_is_help=True
)
@click.option(
	"-p",
	"--path",
	required=True,
	type=click.Path(),
	help="The directory with the file to tail",
)
@click.option(
	"-t",
	"--time-fv",
	required=True,
	default=("min", 30),
	type=(str, int),
	help='The time format and value; the format can be either "hour" or "min", while the value can be between 0 and 60 exclusive.',
)
@logger.catch
def nt_tail(path, time_fv):

	if os.name == "nt":
		logger.error(
			"Sorry! This subcommand cannot be used on windows!"
		)
		exit(1)

	dfl: Callable[..., Any] = field(default_factory=list)

	@dataclass
	class data:
		previous_tail: List[str] = dfl
		current_tail: List[str] = dfl

	d: type = data()
	d.previous_tail = []

	if "hour" in time_fv:
		schedule_func = schedule.every(time_fv[1]).hours.do
	elif "min" in time_fv:
		if not 0 < time_fv[1] < 60:
			logger.error("Sorry! That is an invalid value!")
			exit(1)
		else:
			schedule_func = schedule.every(time_fv[1]).minutes.do
	else:
		logger.error("Sorry! That is not a valid time format!")
		exit(1)

	def inner() -> None:
		resolved_path = fullpath(path, check=True)
		list_of_files = (
			f"{resolved_path}/{_}"
			for _ in os.listdir(resolved_path)
		)
		latest_file = max(list_of_files, key=os.path.getctime)
		d.current_tail = [
			line
			for line in tail(
				{"value": latest_file, "quotes": True}, lines=100
			).stdout
		]
		if d.current_tail != d.previous_tail:
			for line in [_line for _line in d.current_tail if _line not in d.previous_tail]:
				click.echo(line)
			d.previous_tail[:] = d.current_tail[:]

	inner()
	schedule_func(inner)

	while True:
		schedule.run_pending()
		time.sleep(1)


main.add_command(nt_tail)

main()
