# From Imports
from nanite import fullpath, module_installed
from os import sep
from typing import List

# Debug Imports
pout = module_installed("pout")


def path_name(path_list) -> List[str]:
	"""
		Return the class' list of paths joined by periods instead of slashes
	"""
	yield from (
		i.replace(":", "").replace(sep, ".").lstrip(".")
		for i in path_list
	)


def find_excl(path_list) -> str:
	"""
		Return the class' list of paths joined by the "find" command's path exlusion format
	"""
	return f'-path {" -o -path ".join(path_list)}'
