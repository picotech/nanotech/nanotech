# From Imports
from nanite import module_installed

# Debug Imports
pout = module_installed("pout")

# Exception Classes
class Error(Exception):
	pass


class no_size(Error):
	pass


def txt(size):
	"""
		Create text test files using the "base64" program; sizes are:
		- small:      20 files, 10 bytes
		- medium:     20 files, 10 kilobytes
		- large:      10 files, 10 megabytes
		- extralarge: 2  files, 1  gigabyte
	"""
	from bakery import base64, head

	base64.tier_(
		"/dev/urandom",
		"|",
		head.tier_(
			_temp_tiered=True,
			_end_args=(">", "__tier__.txt"),
			bytes="__tier__",
		),
	)

	if size == "small":
		for i in range(1, 21):
			base64(10, i)
	elif size == "medium":
		for i in range(1, 21):
			base64(10000, i)
	elif size == "large":
		for i in range(1, 11):
			base64(10000000, i)
	elif size == "extralarge":
		for i in range(1, 3):
			base64(1000000000, i)
	else:
		raise no_size(f"Sorry, no size {size}")


def bin(size):
	"""
		Create binary test files using the "fallocate" program; sizes are:
		- small:      20 files, 10 bytes
		- medium:     20 files, 10 kilobytes
		- large:      10 files, 10 megabytes
		- extralarge: 2  files, 1  gigabyte
	"""
	from bakery import fallocate

	fallocate.tier_(
		_end_args=("__tier__.bin"), length="10__tier__"
	).join()

	if size == "small":
		for i in range(1, 21):
			fallocate("", i)
	elif size == "medium":
		for i in range(1, 21):
			fallocate("K", i)
	elif size == "large":
		for i in range(1, 11):
			fallocate("M", i)
	elif size == "extralarge":
		for i in range(1, 3):
			fallocate("G", i)
	else:
		raise no_size(f"Sorry, no size {size}")
