# Imports
import os
import pytz

# From Imports
from datetime import datetime
from nanite import fullpath, sui, module_installed
from packaging.version import parse
from sys import executable
from typing import (
	List,
	Tuple,
	Any,
	Generator,
	MutableSequence as MS,
)

# Debug Imports
pout = module_installed("pout")


class Error(Exception):
	pass


class git_not_set(Error):
	pass


def __getattr__(name):
	"""
		Answer 1: https://stackoverflow.com/questions/56786604/import-modules-that-dont-exist-yet/56786875#56786875
		User 1:   https://stackoverflow.com/users/1016216/l3viathan

		Answer 2: https://stackoverflow.com/questions/56786604/import-modules-that-dont-exist-yet/56795585#56795585
		User 2:   https://stackoverflow.com/users/3830997/matthias-fripp

		Answer 3: https://stackoverflow.com/questions/12332975/installing-python-module-within-code/50255019#50255019
		User 3:   https://stackoverflow.com/users/186964/aaron-de-windt
		
		Answer 4: https://stackoverflow.com/a/24773951/10827766
		User 4:   https://stackoverflow.com/users/2108548/rominf

		Modified by me
	"""
	if name == "__path__":
		raise AttributeError

	try:
		return __import__(name)
	except ImportError:
		sui("bakery", executable)("-m pip install", name)
	finally:
		return __import__(name)


def git_info() -> Generator[str, None, None]:
	"""
		Returns a list of GIT environmental values
	"""
	try:
		yield from (
			os.environ["GIT_AUTHOR_NAME"],
			os.environ["GIT_AUTHOR_EMAIL"],
			os.environ["GIT_COMMITTER_NAME"],
			os.environ["GIT_COMMITTER_EMAIL"],
		)
	except KeyError:
		raise git_not_set(
			'Sorry, something happened; check your "GIT" variables.'
		)


def powerset(iterable: List[Any]) -> Generator[Any, None, None]:
	"""
		Credits: https://docs.python.org/3/library/itertools.html#itertools-recipes
		Modified by me
		powerset([1,2,3]) --> [(), (1,) (2,) (3,) (1, 2) (1, 3) (2, 3) (1, 2, 3)]
	"""
	s: Tuple[List[Any]] = tuple(iterable)
	from itertools import chain, combinations

	yield chain.from_iterable(
		combinations(s, r) for r in range(len(s) + 1)
	)


class list_to_file:
	def __init__(self):
		self.list_of_files: List[str] = []

	def __call__(
		self,
		lst: MS[Any],
		tz: str = "Canada/Eastern",
		tmp_dir: str = "/tmp",
	):
		dt_file = fullpath(
			tmp_dir,
			datetime.now(pytz.timezone(tz)).strftime(
				"%Y.%m.%d.%H.%M.%S.%f"
			)
			+ ".tmp",
		)
		self.list_of_files.append(dt_file)
		with open(dt_file, "w+") as file:
			for line in lst:
				file.write(str(line).rstrip() + "\n")
		return dt_file

	def clean(self):
		for file in self.list_of_files:
			os.remove(file)


def gen_incl(
	tz: str = "Canada/Eastern",
	tmp_dir: str = "/tmp",
	path: str = ".",
	ignore_file: str = "",
	ignore_list: MS[Any] = [],
	follow_symlinks: bool = False,
):
	"""
		Generate the list of included directories within the given "path" variable, using the "fd" command, ignoring what's in the "ignore_file" variable, and put it into the file in the "include_file" variable
	"""
	from bakery import fd

	try:

		if not ignore_file:
			ltf = list_to_file()
			ignore_file = ltf(
				ignore_list, tmp_dir=tmp_dir, tz=tz
			)

		yield from (
			fd(
				".",
				follow=follow_symlinks,
				_end_args=[
					{"value": fullpath(path), "quotes": True}
				],
				hidden=True,
				no_ignore_vcs=True,
				absolute_path=True,
				ignore_file=ignore_file,
			).stdout
		)
	finally:
		if not ignore_file:
			ltf.clean()


def gen_incl_excl(
	tz: str = "Canada/Eastern",
	tmp_dir: str = "/tmp",
	path: str = ".",
	inclusion_rules: List[str] = [],
	inclusion_rules_file: str = "",
	exclusion_rules: List[str] = [],
	exclusion_rules_file: str = "",
	follow_symlinks: bool = False,
):
	from collections import namedtuple

	from bakery import fd

	fd.bake_(
		".",
		follow=follow_symlinks,
		_end_args=[{"value": fullpath(path), "quotes": True}],
		hidden=True,
		no_ignore_vcs=True,
		absolute_path=True,
		_type=set,
		_return="stdout",
	)
	total_file_list: set = fd()

	file_lists = namedtuple(
		"file_lists",
		"old_include old_exclude new_include new_exclude",
	)

	def return_generators(iterable):
		yield from iterable

	try:

		if (
			not inclusion_rules_file
			or os.stat(inclusion_rules_file).st_size == 0
		):
			ltfi: type = list_to_file()
			inclusion_rules_file: str = ltfi(
				inclusion_rules, tmp_dir=tmp_dir, tz=tz
			)

		inclusion_rules_file = dict(
			value=fullpath(inclusion_rules_file), quotes=True
		)

		if (
			not exclusion_rules_file
			or os.stat(exclusion_rules_file).st_size == 0
		):
			ltfe: type = list_to_file()
			exclusion_rules_file: str = ltfe(
				exclusion_rules, tmp_dir=tmp_dir, tz=tz
			)

		exclusion_rules_file = dict(
			value=fullpath(exclusion_rules_file), quotes=True
		)

		# For fd_[in|ex]cluded_file_list, I need to subtract the fd list from the total file list because otherwise
		# it first applies the exclusion rules and then the inclusion rules, not the other way around.

		# fd_included_file_list: set = total_file_list - fd(
		# 	ignore_file=inclusion_rules_file
		# )

		# fd_excluded_file_list: set = total_file_list - fd(
		# 	ignore_file=exclusion_rules_file
		# )

		fd_included_file_list: set = fd(
			ignore_file=exclusion_rules_file
		)

		fd_excluded_file_list: set = fd(
			ignore_file=inclusion_rules_file
		)

		nifl: set = fd_included_file_list - fd_excluded_file_list
		# # new_include_file_list: set = fd_included_file_list if not nifl else nifl
		new_include_file_list: set = nifl

		return file_lists(
			old_include=fd_included_file_list,
			new_include=(new_include_file_list),
			old_exclude=fd_excluded_file_list,
			new_exclude=(
				total_file_list - new_include_file_list
			),
		)

	finally:
		if os.stat(inclusion_rules_file[0]).st_size == 0:
			ltfi.clean()
		if os.stat(exclusion_rules_file[0]).st_size == 0:
			ltfe.clean()


def wsl_version():
	if "microsoft" in {
		os.uname().release.lower(),
		os.uname().version.lower(),
	}:
		if (
			parse("3.4.0")
			< parse(os.uname().release.split("-")[0])
			< parse("4.19.43")
		):
			return 1
		else:
			return 2
	else:
		return 0


def install_import(module: str):
	"""
		Answer 1: https://stackoverflow.com/questions/12332975/installing-python-module-within-code/50255019#50255019
		User 1:   https://stackoverflow.com/users/186964/aaron-de-windt
		
		Answer 2: https://stackoverflow.com/a/24773951/10827766
		User 2:   https://stackoverflow.com/users/2108548/rominf

		Answer 3: https://stackoverflow.com/questions/43768428/how-can-i-implicitly-pass-the-current-global-namespace-in-python/43768646#43768646
		User 3:   https://stackoverflow.com/users/674925/erez

		Modified by me
	"""
	# Imports
	import inspect

	try:
		inspect.stack(1)[1][0].f_globals[module] = __import__(
			module
		)
	except ImportError:
		sui("bakery", executable)("-m pip install", name)
	finally:
		inspect.stack(1)[1][0].f_globals[module] = __import__(
			module
		)


def sn_to_float(sn) -> int:
	base, power = sn.split("e+")
	return round(float(base + ("0" * int(power))))

