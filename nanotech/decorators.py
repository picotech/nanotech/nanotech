# Imports
import inspect
import operator

# From Imports
from threading import Thread
from typing import (
	Union,
	Callable,
	Any,
	Dict,
	Tuple,
	MutableSequence as MS,
)


class Error(Exception):
	pass


class no_conditions(Error):
	pass


class too_many_operators(Error):
	pass


class thread_off_class:
	def __init__(self, func, args, kwargs):
		self.func = func
		self.args = args
		self.kwargs = kwargs

	def start(self):
		self.output = self.func(*self.args, **self.kwargs)

	def join(self):
		pass


class base:
	def __init__(self):
		pass

	def _inner(self):
		if self.thread_class:
			if self.thread_on:
				setattr(
					self.thread_class,
					self.func.__name__,
					Thread(
						target=self.func,
						args=self.args,
						kwargs=self.kwargs,
					),
				)
			else:
				setattr(
					self.thread_class,
					self.func.__name__,
					thread_off_class(
						self.func, self.args, self.kwargs
					),
				)
			getattr(
				self.thread_class, self.func.__name__
			).start()
			self.output = (
				(
					getattr(
						getattr(
							self.thread_class, self.func.__name__
						),
						"output",
					)
				)
				if not self.thread_on
				else None
			)
		else:
			self.output = self.func(*self.args, **self.kwargs)


def rt(
	func: Callable[..., Any],
	args: Tuple[Any] = (),
	thread_on: bool = True,
	kwargs: Dict[str, Any] = {},
):
	_ = (
		Thread(target=func, args=args, kwargs=kwargs)
		if thread_on
		else thread_off_class(func, args, kwargs)
	)
	_.start()
	return _


class zoom_savitar_class(base):
	def __init__(
		self,
		func: Callable[..., Any],
		args: Tuple[Any],
		kwargs: Dict[str, Any],
		thread_class: Union[type, None] = None,
		thread_on: bool = False,
	) -> Any:
		self.func = func
		self.args = args
		self.thread_class = thread_class
		self.thread_on = thread_on
		self.kwargs = kwargs
		super().__init__()

	def __call__(self):
		self._inner()


def zoom(
	*args,
	thread_class: Union[type, None] = None,
	thread_on: bool = False,
	**kwargs,
) -> type:
	def wrapper(func):
		_ = zoom_savitar_class(
			func, args, kwargs, thread_class, thread_on
		)
		_()
		return _

	return wrapper


def zoom_dep(
	*args,
	thread_class: Union[type, None] = None,
	thread_on: bool = False,
	**kwargs,
) -> Any:
	"""
		Run a function
	"""

	def wrapper(func: Callable[..., Any]):
		if thread_class:
			if thread_on:
				setattr(
					thread_class,
					func.__name__,
					Thread(
						target=func, args=args, kwargs=kwargs
					),
				)
			else:
				setattr(
					thread_class,
					func.__name__,
					thread_off_class(),
				)
			getattr(thread_class, func.__name__).start()
		else:
			func(*args, **kwargs)

	return wrapper


def savitar(
	*args: Tuple[Any],
	thread_class: Union[type, None] = None,
	thread_on: bool = False,
	**kwargs: Dict[str, Any],
) -> type:
	def wrapper(func: Callable[..., Any]):
		return zoom_savitar_class(
			func, args, kwargs, thread_class, thread_on
		)

	return wrapper


def savitar_dep(
	*args,
	thread_class: Union[type, None] = None,
	thread_on: bool = False,
	**kwargs,
) -> Callable[..., Any]:
	"""
		Run a function later
	"""

	def wrapper(func: Callable[..., Any]):
		from functools import wraps

		@wraps(func)
		# This returns the function of the function, not the call of the function
		def wrapped():
			if thread_class:
				if thread_on:
					setattr(
						thread_class,
						func.__name__,
						Thread(
							target=func, args=args, kwargs=kwargs
						),
					)
				else:
					setattr(
						thread_class,
						func.__name__,
						thread_off_class(),
					)
				getattr(thread_class, func.__name__).start()
			else:
				func(*args, **kwargs)

		return wrapped

	return wrapper


class godspeed_class(base):
	def __init__(
		self,
		func: Callable[..., Any],
		args: Tuple[Any],
		kwargs: Dict[str, Any],
		thread_class: Union[type, None] = None,
		thread_on: bool = False,
		_lt: Union[MS[Any], None] = None,
		_le: Union[MS[Any], None] = None,
		_eq: Union[MS[Any], None] = None,
		_ne: Union[MS[Any], None] = None,
		_gt: Union[MS[Any], None] = None,
		_ge: Union[MS[Any], None] = None,
		_not_: Union[MS[Any], None] = None,
		_is: Union[MS[Any], None] = None,
		_is_not: Union[MS[Any], None] = None,
		_truth: Union[MS[Any], None] = None,
		_and_: Union[MS[Any], None] = None,
		_or_: Union[MS[Any], None] = None,
	):
		self.func: Callable[..., Any] = func
		self.args: Tuple[Any] = args
		self.kwargs: Dict[str, Any] = kwargs
		self.thread_class: Union[type, None] = thread_class
		self.thread_on: bool = thread_on
		self.original_operator_kwargs: Dict[str, Any] = {
			"_lt": _lt,
			"_le": _le,
			"_eq": _eq,
			"_ne": _ne,
			"_gt": _gt,
			"_ge": _ge,
			"_not_": _not_,
			"_is": _is,
			"_is_not": _is_not,
			"_truth": _truth,
			"_and_": _and_,
			"_or_": _or_,
		}
		self.single_operator_kwargs: Tuple[str] = tuple(
			("_not_", "_truth")
		)
		self.new_operator_kwargs: Dict[str, Any] = {
			key: value
			for key, value in self.original_operator_kwargs.items()
			if (
				value is not None
				and (
					len(value) == 2
					or (
						len(value) == 1
						and key in self.single_operator_kwargs
					)
				)
			)
		}
		super().__init__()

	def __call__(self):
		if not self.new_operator_kwargs:
			raise no_conditions(
				"Sorry; conditions must be supplied!"
			)
		if len(self.new_operator_kwargs) > 1:
			raise too_many_operators(
				"Sorry; no more than one operator argument may be used!"
			)
		if getattr(
			operator,
			tuple(self.new_operator_kwargs.keys())[0][1::],
		)(*(tuple(self.new_operator_kwargs.values()))[0]):
			self.if_output = self._inner()
		else:
			self.func, self.args, self.kwargs = (
				self.else_tup
				if hasattr(self, "else_tup")
				else (lambda: None, (), {})
			)
			self.else_output = self._inner()

	def else_(self, *args, **kwargs):
		def wrapper(func):
			self.else_tup = (func, args, kwargs)
			return self

		return wrapper


# Add all operators that make sense
def godspeed(
	*args: Tuple[Any],
	thread_class: Union[type, None] = None,
	thread_on: bool = False,
	_run: bool = False,
	_lt: Union[MS[Any], None] = None,
	_le: Union[MS[Any], None] = None,
	_eq: Union[MS[Any], None] = None,
	_ne: Union[MS[Any], None] = None,
	_gt: Union[MS[Any], None] = None,
	_ge: Union[MS[Any], None] = None,
	_not_: Union[MS[Any], None] = None,
	_is: Union[MS[Any], None] = None,
	_is_not: Union[MS[Any], None] = None,
	_truth: Union[MS[Any], None] = None,
	_and_: Union[MS[Any], None] = None,
	_or_: Union[MS[Any], None] = None,
	**kwargs: Dict[str, Any],
) -> Union[type, Callable[..., Any]]:
	"""
		Run a function immediately or later with a condition
	"""

	def wrapper(func: Callable[..., Any]):
		"""
			"_run" can only be used if no "else_" function is defined.
		"""
		_ = godspeed_class(
			func,
			args,
			kwargs,
			thread_class,
			thread_on,
			_lt,
			_le,
			_eq,
			_ne,
			_gt,
			_ge,
			_not_,
			_is,
			_is_not,
			_truth,
			_and_,
			_or_,
		)
		if _run:
			_()
		return _

	return wrapper


# A class decorator...?
