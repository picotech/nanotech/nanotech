# nanotech

***

## TODO

- [x] Make More Global
- [ ] Make Wiki
- [x] `N/A` Cythonize...?
- [ ] Create a class decorator

***

## Links

* `Wiki:` [Notion: Nanotech](https://www.notion.so/picotech/Nanotech)

***

## Tags

[![](tags/project/nanotech.svg)](https://github.com/shadowrylander/nanotech)

| Platform | Type | Language | Modules Used |
| - | - | - | - |
| ![](tags/platform/all.svg) | ![](tags/type/module.svg) | [![](tags/language/python3.6.0.svg)](https://www.python.org/downloads/release/python-360/) | [![](tags/modules_used/click.svg)](https://click.palletsprojects.com/en/7.x/) |
|  |  |  | [![](tags/modules_used/collections.svg)](https://docs.python.org/3/library/collections.html) |
|  |  |  | [![](tags/modules_used/dataclasses.svg)](https://docs.python.org/3/library/dataclasses.html) |
|  |  |  | [![](tags/modules_used/datetime.svg)](https://docs.python.org/3/library/datetime.html) |
|  |  |  | [![](tags/modules_used/functools.svg)](https://docs.python.org/3/library/functools.html) |
|  |  |  | [![](tags/modules_used/imohash.svg)](https://github.com/kalafut/py-imohash) |
|  |  |  | [![](tags/modules_used/itertools.svg)](https://docs.python.org/3/library/itertools.html) |
|  |  |  | [![](tags/modules_used/loguru.svg)](https://github.com/Delgan/loguru) |
|  |  |  | [![](tags/modules_used/operator.svg)](https://docs.python.org/3/library/operator.html) |
|  |  |  | [![](tags/modules_used/os.svg)](https://docs.python.org/3/library/os.html) |
|  |  |  | [![](tags/modules_used/packaging.svg)](https://packaging.pypa.io/en/latest/) |
|  |  |  | [![](tags/modules_used/pout.svg)](https://github.com/Jaymon/pout) |
|  |  |  | [![](tags/modules_used/pytz.svg)](http://pytz.sourceforge.net/) |
|  |  |  | [![](tags/modules_used/schedule.svg)](https://schedule.readthedocs.io/en/stable/) |
|  |  |  | [![](tags/modules_used/sys.svg)](https://docs.python.org/3/library/sys.html) |
|  |  |  | [![](tags/modules_used/threading.svg)](https://docs.python.org/3/library/threading.html) |
|  |  |  | [![](tags/modules_used/toml.svg)](https://github.com/toml-lang/toml) |
|  |  |  | [![](tags/modules_used/typing.svg)](https://docs.python.org/3/library/typing.html) |
|  |  |  | [![](tags/modules_used/time.svg)](https://docs.python.org/3/library/time.html) |
|  |  |  | [![](tags/modules_used/xxhash.svg)](https://pypi.org/project/xxhash/) |

***

## Notes

*

***

## Content

&nbsp;&nbsp;&nbsp;&nbsp; [`Nanotech`](https://pypi.org/project/nanotech/) is a set of modules (in this case a python module) which others may find useful (doubtful); they are, however, at the moment, mostly script-based.

***

## Saku

<!-- saku start -->

### install

    python3 -m pip install .

<!-- saku end -->

***

## Copyright
